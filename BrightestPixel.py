"""
    ---Brightest Pixel---

Program for plotting flux vs. time curve of brightest pixel from images from set of .fits files;
Classes:    PictureSet;
Functions:  main, getFluxCurve;

Argument 1: full name of text file with full names of used .fits files;
Argument 2: full name of result .pdf file
:Example:

>> python BrightestPixel.py filenames.txt result.pdf


Requires:
    astropy
    numpy
    matplotlib
"""

from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from datetime import timedelta, datetime
import sys

class PictureSet(object):
    """
    class PictureSet:
    Operating with data in .fits files;

    Initialization: .fits file which contains set of 2-d float32 arrays and information about them;
    Usage: Building table (time - value) for flux of brightest pixel;
    """

    def __init__(self, filename):
        """
        Initialization function;
        Opens .fits file, makes NaN values filtration, gets observation start time and delay between pictures.
        Initialization of brightest pixel coordinates variables;
        :var self.f:            Input .fits file variable;
        :var self.setOfPic:     Array of 2-d numpy.array - images;
        :var self.timeStart:    Time location of first frame;
        :var self.timeDelay:    Delay between frames;
        :var self.timeArr:      Array of frames time locations;
        :var self.brightestCoordinate_x:    Raw position of brightest pixel in 0 frame;
        :var self.brightestCoordinate_y:    Column position of brightest pixel in 0 frame;
        :except FileNotFoundError:          Stops initialisation if file opening error appears.
        :param filename:        full name of input file;
        :return None:
        """
        try:
            self.f = fits.open(filename)
        except FileNotFoundError:
            print('file', filename, 'not found')
            exit()

        # Get only full not-NaN rows
        self.setOfPic = [np.array([j for j in i if not np.isnan(j).any()]) for i in self.f[0].data]

        # Getting time from header (converting num of seconds between 00:00 1980-01-01 and
        # 12:00 2000-01-01 (J2000.0) (30 years + 12 hours + 7 leap days))
        timeStart = datetime.fromtimestamp(self.f[0].header['UTCS_OBS']) + timedelta(days=365*30+7, hours=12)
        timeDelay = timedelta(seconds=float(self.f[0].header['AORTIME']))
        self.timeArr = np.array([timeStart + i*timeDelay for i in range(len(self.setOfPic))])

        # Getting coordinates of pixel with maximum value
        self.brigtestCoord_x, self.brigtestCoord_y = self.getBrightestCoord(0)

        self.f.close()

    def getBrightestCoord(self, index):
        """
        Uses numpy.array.argmax() function to find indices of element with maximum value in frame №<index>;
        :param index: index of frame used is self.setOfPic;
        :var pic: Current frame;
        :var sh: Size of pic;
        :return tuple (x, y): coordinates of max value;
        """
        pic = np.array(self.setOfPic[index])
        sh = pic.shape
        return divmod(pic.argmax(), sh[1])

    def getFluxCurve(self):
        """
        Returns data for plotting;
        :var fluxArr: Array of brightest pixels' flux values;
        :return numpy.array fluxArr: Array of brightest pixel flux;
        :return numpy.array timeArr: Array of time moments;
        :rtype: tuple (numpy.array, numpy.array)
        """
        fluxArr = np.array([i[self.brigtestCoord_x, self.brigtestCoord_y] for i in self.setOfPic])
        return fluxArr, self.timeArr


def getPlot(filename, arrayOfPicSets):
    """
    Making plot and export it to .pdf file;
    :param filename: name of result file;
    :param arrayOfPicSets: array of PictureSet objects;
    :return: None;
    """

    fig = plt.figure()
    ax = fig.add_subplot(111)

    fluxArr = np.array([])
    timeArr = np.array([])

    # Collecting data for plotting from all elements of input array
    for i in arrayOfPicSets:
        cur = i.getFluxCurve()
        fluxArr = np.hstack((fluxArr, cur[0]))
        timeArr = np.hstack((timeArr, cur[1]))

    # making plot
    ax.plot(timeArr, fluxArr)

    # Setting axis labels
    plt.rcParams.update({'font.size': 9})
    ax.set_xlabel('time')
    ax.set_ylabel('flux')
    ax.set_title('Flux vs. Time Plot for pixel with maximum value')
    ax.legend(['Pixel flux with coordinate (%i, %i)' %
              (arrayOfPicSets[0].brigtestCoord_x, arrayOfPicSets[0].brigtestCoord_y)])

    # export to <fileName>.pdf file
    pp = PdfPages(filename)
    plt.savefig(pp, format='pdf')
    pp.close()


def main():
    '''
    Main function.
    Creating array of objects of PictureSet. Making plot.
    :return None:
    '''
    if len(sys.argv) < 3:
        if sys.argv[1] == '-h':
            print(__doc__)
            exit()
        raise IndexError
    fictFiles = open(sys.argv[1], 'r')
    arrPS = [PictureSet(i[:-1]) for i in fictFiles]
    getPlot(sys.argv[2], arrPS)

try:
    main()
except IndexError:
    print('Not enough parameters(2 required - %i given) (-h to show info)' % (len(sys.argv)-1))
    exit()